from random import randint

name_user = input("Hi! What is your name? ")

month = randint(1, 12)
year = randint(1924, 2004)

num = 1
for guess_number in range(5):
    guess_number = (month, "/", year)
    guess_statement = print("Guess", num, name_user,
                            "were you born in month", month, "/ year", year)
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        break
    elif num == 5:
        print("I have other things to do. You WIN!!!")
    elif response == "no":
        print("Drat! Let me try again")
        num = num + 1
